type Barcode = {
  code: string
  format: string
}

type InputStream = {
  type: 'LiveStream' | 'ImageStream' | 'VideoStream',
  name?: string,
  target: string,
  constraints?: any
}

type QuaggaConfig = {
  inputStream: InputStream,
  locator: any,
  frequency: number
  decoder: any,
  locate: boolean,
  debug: boolean,
  numOfWorkers: number
}
