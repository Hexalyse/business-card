type CameraInfo = {
  deviceId: string
  label: string
}
