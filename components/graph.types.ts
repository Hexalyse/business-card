export class Graph {
  name: String
  type?: GraphType
  points?: GraphDataPoint[]
  areas?: GraphDataArea[]
}
export class GraphDataPoint {
  value: number
  type: GraphPointType
  data?: DataField[]
  date: Date
}
export class GraphDataArea {
  value: number
  type: GraphAreaType
  data?: DataField[]
  start: Date
  end: Date
}
export enum GraphType {
  HISTORY = 'HISTORY',
  PRODUCTIVITY = 'PRODUCTIVITY',
  MOBILE = 'MOBILE'
}
export enum GraphPointType {
  START = 'START',
  STOP = 'STOP',
  UPDATE = 'UPDATE',
  WORKERS = 'WORKERS'
}
export enum GraphAreaType {
  LOST = 'LOST',
  PAUSE = 'PAUSE',
  DOWNTIME = 'DOWNTIME'
}

export type DataField = {
  _id?: ObjectId
  key: string
  value: string
}

export type ObjectId = string
