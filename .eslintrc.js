// https://eslint.org/docs/user-guide/configuring
module.exports = {
  root: true,
  parserOptions: {
    project: './tsconfig.json',
    extraFileExtensions: ['.vue'],
    ecmaFeatures: {
      legacyDecorators: true,
      emitDecoratorMetadata: true,
      experimentalDecorators: true
    }
  },
  env: {
    browser: true,
  },
  extends: [
    '@nuxtjs/eslint-config-typescript',
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to plugin:vue/strongly-recommended or plugin:vue/recommended for stricter rules.
    'plugin:vue/recommended',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add your custom rules here
  rules: {
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-unused-vars': 'off',
    'vue/return-in-computed-property': 'off',
    'vue/require-prop-type-constructor': 'off',
    'no-unused-expressions': 'off'
  }
}
